## Project setup

1. Set env variables for email server in `docker-compose.yml`
2. ```docker-compose up```
3. Open your browser at http://localhost:8080/


- Register to the app
- User will be saved in mongo db with hashed password and jwt will be issued and saved by frontend in localStorage 
  (can be more secured, I know. But, for purpose of this and for sake of simplicity localStorage fits best)
- You are able to send and receive messages.
- Messages are sent over REST API just for matter of using various approaches. Websocket could be used as well.
- All received messages will be saved to DB by backend and forwarded to all connected web sockets
- Any other user who joins the app will see the most recent messages (in last 24h)
- Secured endpoints:
  - Fetching notifications
  - Sending notifications 
  - Connecting to web socket)
- Public endpoints:
  - Login 
  - Registration
- Postman was used for testing purposes. Environment and collection can be found in `./postman`
- Frontend app created in Vue.js is also available for testing.


The way of how to achieve notification is not defined in task description, so I chose web sockets.
There is a way to use mail notifications. In that case I would use mail-service.js to send notifications to user's 
email. Another solution could be push notifications for mobile devices using firebase.