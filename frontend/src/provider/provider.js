import axios from 'axios';

const client = axios.create({
  baseURL: 'http://localhost:3000/igp-app', // Ažurirajte s vašim baseURL-om
});

client.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('userToken');
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

function saveUserInfo(response) {
  localStorage.setItem('userToken', response.data.token);
  localStorage.setItem('isAuthenticated', 'true');
  localStorage.setItem('username', response.data.username);
}

export const login = async (username, password) => {
  try {
    const response = await client.post('/login', {
      username: username,
      password: password,
    });
    saveUserInfo(response);
    return response;
  } catch (error) {
    alert('Login error');
  }
};

export const register = async (username, email, password) => {
  try {
    let response = await client.post('/register', {
      username: username,
      email: email,
      password: password,
    });
    console.log(response)
    saveUserInfo(response);

    return response;
  } catch (error) {
    alert('Registration error.');
  }
};

export const notify = async (message) => {
  await client.post('/notify', {
    message,
  });
};

export const notifications = async () => {
  return await client.get('/notifications');
};
