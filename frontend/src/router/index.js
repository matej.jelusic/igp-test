import { createRouter, createWebHistory } from 'vue-router';
import UserRegistration from '@/components/UserRegistration.vue';
import UserLogin from '@/components/UserLogin.vue';
import HomePage from '@/components/HomePage.vue';
import NotificationSender from '@/components/NotificationSender.vue';

const routes = [
  { path: '/', component: HomePage, meta: { requiresAuth: true } },
  { path: '/login', component: UserLogin },
  { path: '/register', component: UserRegistration },
  {
    path: '/notification',
    component: NotificationSender,
    meta: { requiresAuth: true },
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  // Provjerite je li ruta zaštićena i korisnik nije prijavljen
  if (
    to.matched.some((record) => record.meta.requiresAuth) &&
    'true' !== localStorage.getItem('isAuthenticated')
  ) {
    // Korisnik nije prijavljen, preusmjeri na stranicu za prijavu
    next('/login');
  } else {
    // Nastavi s rutom
    next();
  }
});

export default router;
