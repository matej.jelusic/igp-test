export const CustomException = (message, statusCode) => {
    const error = new Error(message);

    error.statusCode = statusCode;
    return error;
};

CustomException.prototype = Object.create(Error.prototype);
