export const errorHandler = async (error, req, res, next) => {
    console.log(error.message)
    res.status(error.statusCode || 500).send(error.message);
};
