import nodemailer from 'nodemailer';

export const sendWelcomeEmail = async (userEmail, username) => {
  let transporter = nodemailer.createTransport({
    host: process.env.EMAIL_SERVER_HOST,
    port: process.env.EMAIL_SERVER_PORT,
    secure: false,
    auth: {
      user: process.env.EMAIL_SERVER_USERNAME,
      pass: process.env.EMAIL_SERVER_PASSWORD,
    },
  });

  let info = await transporter.sendMail({
    from: process.env.EMAIL_SERVER_FROM_MAIL,
    to: userEmail,
    subject: 'Welcome to iGP test!',
    text: `Hello ${username}, welcome to iGP test!`,
    html: `<b>Pozdrav ${username}, dobrodošli u iGP test!</b>`
  });

  console.log('Message sent: %s', info.messageId);
};
