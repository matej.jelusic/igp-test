import { WebSocketServer } from 'ws';
import http from 'http';
import jwt from 'jsonwebtoken';

export const connectedClients = new Set();

export const startWebSocketServer = (app) => {
  const wss = new WebSocketServer({ noServer: true });

  wss.on('connection', function connection(ws) {
    connectedClients.add(ws);
    console.log('Connected: Authorized client');

    ws.on('close', function () {
      connectedClients.delete(ws);
    });
  });

  const server = http.createServer(app);

  server.on('upgrade', function upgrade(request, socket, head) {
    const url = new URL(request.url, `http://${request.headers.host}`);
    const token = url.searchParams.get('token');

    if (!token) {
      socket.destroy();
      return;
    }

    jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
      if (err || !decoded) {
        socket.destroy();
        return;
      }

      wss.handleUpgrade(request, socket, head, function done(ws) {
        wss.emit('connection', ws, request);
      });
    });
  });

  server.listen(30001);
};
