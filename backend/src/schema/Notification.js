import mongoose from 'mongoose';

const NotificationSchema = new mongoose.Schema({
  sender: { type: String, required: true },
  date: { type: Date, required: true },
  message: { type: String, required: true },
});

const Notification = mongoose.model('Notification', NotificationSchema);
export { Notification };
