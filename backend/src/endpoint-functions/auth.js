import bcrypt from 'bcrypt';
import { User } from '../schema/User.js';
import { sendWelcomeEmail } from '../service/mail-service.js';
import jwt from 'jsonwebtoken';
import { CustomException } from '../helper/custom-exception.js';

export const register = async (req, res, next) => {
  try {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);

    const user = new User({
      username: req.body.username,
      email: req.body.email,
      password: hashedPassword,
    });

    const savedUser = await user.save();

    await sendWelcomeEmail(savedUser.email, savedUser.username);

    const token = generateJwt(user.username);

    res.status(201).json({ username: user.username, token });
  } catch (error) {
    console.error(error.message);
    next(CustomException(error.message, 500));
  }
};

export const login = async (req, res, next) => {
  const user = await User.findOne({ username: req.body.username });
  if (user == null) {
    next(CustomException(`User ${req.body.username} not found`, 400));
  }

  try {
    if (await bcrypt.compare(req.body.password, user.password)) {
      const token = generateJwt(user.username);
      res.json({ username: user.username, token });
    } else {
      res.send('Login failed', 401);
    }
  } catch (error) {
    next(CustomException(error.message, 401));
  }
};

export const authenticateToken = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (token == null) return res.sendStatus(401);

  jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);
    req.user = user;
    next();
  });
};

const generateJwt = (username) => {
  return jwt.sign({ username }, process.env.JWT_SECRET, {
    expiresIn: '24h',
  });
};
