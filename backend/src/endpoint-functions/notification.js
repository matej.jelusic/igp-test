import { connectedClients } from '../wss-server.js';
import { Notification } from '../schema/Notification.js';
import { CustomException } from '../helper/custom-exception.js';

export const notify = async (req, res) => {
  for (const ws of connectedClients) {
    const notification = new Notification({
      sender: req.user.username,
      message: req.body.message,
      date: Date.now(),
    });

    let savedNotification = await notification.save();

    ws.send(JSON.stringify(savedNotification));
  }

  res.send('Message sent', 200);
};

export const fetchRecentNotifications = async (req, res, next) => {
  try {
    const dayAgo = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);

    let notifications = await Notification.find({
      date: { $gte: dayAgo },
    })
      .sort({ date: 1 })
      .exec();
    res.send(notifications, 200);
  } catch (error) {
    console.error('Error fetching notifications:', error);
    next(
      CustomException(`Error fetching notifications: ${error.message}`, 500)
    );
  }
};
