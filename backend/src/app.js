import express from 'express';
import dotenv from 'dotenv';
import { errorHandler } from './middleware/error-handler.js';
import {
  authenticateToken,
  login,
  register,
} from './endpoint-functions/auth.js';
import {
  fetchRecentNotifications,
  notify,
} from './endpoint-functions/notification.js';
import { startWebSocketServer } from './wss-server.js';
import cors from 'cors';
import helmet from 'helmet';

dotenv.config();
const app = express();
const router = express.Router();

app.use(helmet());
app.use(
  cors({
    origin: 'http://localhost:8080',
  })
);

app.use('/igp-app', router);

router.use(express.json());
router.post('/register', register);
router.post('/login', login);
router.post('/notify', authenticateToken, notify);
router.get('/notifications', authenticateToken, fetchRecentNotifications);

app.use(errorHandler);

startWebSocketServer(app);
export default app;
