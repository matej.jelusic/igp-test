import app from './app.js';
import mongoose from 'mongoose';

mongoose.connect(
  `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    authSource: 'admin', // Specifies the database name associated with the user's credentials
  }
);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log('***** iGP test API Started *****');
  console.log(`Port ${port}`);
});
